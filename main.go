package main

import (
	"fmt"
	"golang.org/x/sync/errgroup"
	"sync"
	"time"
)

type OfferDocument struct {
	Region int
}

type numsCol struct {
	Nums []int
}

func (c *numsCol) Add(n int) {
	c.Nums = append(c.Nums, n)
}

func (c *numsCol) Count() int {
	return len(c.Nums)
}

func main() {
	wg := sync.WaitGroup{}
	wg.Add(2)

	go standardProducerConsumer(&wg)
	go errGroupProducerConsumer(&wg)

	wg.Wait()
}

func errGroupProducerConsumer(mainWG *sync.WaitGroup) {
	fmt.Println("ERRGROUP PRODUCER%CONSUMER - RUN")
	response := &numsCol{}

	c := 100
	ch := make(chan int)
	consumerDone := make(chan bool)

	go consumer(consumerDone, ch, response)
	go producerErrGroup(ch, c)

	<-consumerDone

	fmt.Printf("\nCOUNT:%d\n", response.Count())

	mainWG.Done()
}

func producerErrGroup(ch chan int, c int) {
	numbers := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	wg := new(errgroup.Group)

	defer close(ch) // producer done, could close channel
	defer fmt.Println("PRODUCER DONE")
	defer fmt.Println("CHANNEL-CLOSING")

	for i := 0; i < c; i++ {
		for _, number := range numbers {
			n := number // pass to local scope

			wg.Go(func() error {
				// mock some heavy lifting
				time.Sleep(100 * time.Millisecond)
				ch <- n

				return nil
			})
		}
	}

	// wait till
	if err := wg.Wait(); err != nil {
		panic(err.Error())
	}
}

func standardProducerConsumer(mainWG *sync.WaitGroup) {
	fmt.Println("STANDARD PRODUCER%CONSUMER - RUN")
	response := &numsCol{}

	c := 100
	ch := make(chan int)
	consumerDone := make(chan bool)

	go consumer(consumerDone, ch, response)
	go producer(ch, c)

	<-consumerDone

	fmt.Printf("\nCOUNT:%d\n", response.Count())

	mainWG.Done()
}

func consumer(doneChannel chan bool, ch chan int, response *numsCol) {
	for n := range ch {
		//fmt.Println(n)
		response.Add(n)
	}
	// this will fire at channel close
	fmt.Println("CONSUMER DONE")
	fmt.Println("CHANNEL-CLOSED")
	doneChannel <- true
}

func producer(ch chan int, c int) {
	numbers := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	wg := sync.WaitGroup{}

	defer close(ch) // producer done, could close channel
	defer fmt.Println("PRODUCER DONE")
	defer fmt.Println("CHANNEL-CLOSING")

	for i := 0; i < c; i++ {
		for _, number := range numbers {
			wg.Add(1)
			n := number
			go func() {
				// mock some heavy lifting
				time.Sleep(100 * time.Millisecond)
				ch <- n
				wg.Done()
			}()
		}
	}

	wg.Wait() // wait till all
}
